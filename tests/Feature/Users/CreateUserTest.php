<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Faker\Factory;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    /** @test */
    public function authenticated_and_has_permission_user_can_create_new_user()
    {
        $this->withoutExceptionHandling();
        $this->adminIsLogin();
        $createData = [
            'phone' => '0123456789',
            'name' => Factory::create()->name(),
            'email' => Factory::create()->unique()->safeEmail(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
        ];
        $response = $this->postJson('/api/users', $createData);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('status', Response::HTTP_OK)
                    ->where('message', 'Create user successfully!!!')
                    ->etc()
                );
    }

    /** @test */
    public function unauthenticated_user_can_not_create_new_user()
    {
        $createData = [
            'name' => 'root',
            'email' => 'root@gmail.com',
            'password' => '123456789',
            'phone' => '0123456789',
        ];
        $response = $this->postJson('/api/users', $createData);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function user_can_not_create_new_user_if_email_field_is_null()
    {
        $this->withoutExceptionHandling();
        $this->adminIsLogin();
        $createData = [
            'name' => 'root',
            'password' => '123456789',
            'phone' => '0123456789',
        ];
        $response = $this->postJson('/api/users', $createData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function user_can_not_create_new_user_if_email_address_already_in_use()
    {
        $this->withoutExceptionHandling();
        $this->adminIsLogin();
        $createData = [
            'name' => 'root',
            'email' => 'root@gmail.com',
            'password' => '123456789',
            'phone' => '0123456789',
        ];
        $response = $this->postJson('/api/users', $createData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function user_can_not_create_new_user_if_password_field_is_null()
    {
        $this->withoutExceptionHandling();
        $this->adminIsLogin();
        $createData = [
            'name' => 'root',
            'email' => 'root@gmail.com',
            'phone' => '0123456789',
        ];
        $response = $this->postJson('/api/users', $createData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function user_can_not_create_new_user_if_password_has_less_than_limit_characters()
    {
        $this->withoutExceptionHandling();
        $this->adminIsLogin();
        $createData = [
            'name' => 'root',
            'email' => 'root@gmail.com',
            'password' => '123456',
            'phone' => '0123456789',
        ];
        $response = $this->postJson('/api/users', $createData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function user_can_not_create_new_user_if_phone_is_not_number_and_length_less_than_ten()
    {
        $this->withoutExceptionHandling();
        $this->adminIsLogin();
        $createData = [
            'name' => 'root',
            'email' => 'root@gmail.com',
            'password' => '123456789',
            'phone' => 'abc1234',
        ];
        $response = $this->postJson('/api/users', $createData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
