<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Passport\Passport;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_update_user_account()
    {
        $this->withoutExceptionHandling();
        $user = $this->createUser();
        $this->userIsLogin();
        $updateData = [
            'name' => 'admin',
            'phone' => '0123456789',
        ];
        $response = $this->json('PUT','/api/users/'.$user->id, $updateData);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn (AssertableJson $json) =>
            $json->where('status', Response::HTTP_OK)
                ->where('message', 'Update successfully')
                ->etc()
            );
    }

    /** @test */
    public function unauthenticated_user_can_not_update_user_account()
    {
        $user = $this->createUser();
        $updateData = [
            'name' => 'root',
            'email' => 'root@root.com',
        ];
        $response = $this->json('PUT','/api/users/'.$user->id, $updateData);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function user_can_not_update_user_account_if_email_address_update_already_in_use()
    {
        $user = $this->createUser();
        $this->userIsLogin();
        $updateData = [
            'name' => 'root',
            'email' => 'root@root.com',
        ];
        $response = $this->json('PUT','/api/users/'.$user->id, $updateData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function user_can_not_update_user_if_password_update_has_less_than_limit_characters()
    {
        $this->withoutExceptionHandling();
        $user = $this->createUser();
        $this->userIsLogin();
        $updateData = [
            'name' => 'root',
            'password' => '123456',
        ];
        $response = $this->json('PUT','/api/users/'.$user->id, $updateData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function user_can_not_update_user_if_phone_update_is_not_number_and_length_less_than_ten()
    {
        $this->withoutExceptionHandling();
        $user = $this->createUser();
        $this->userIsLogin();
        $updateData = [
            'email' => 'root@root.com',
            'phone' => 'abc1234',
        ];
        $response = $this->json('PUT', '/api/users/'.$user->id, $updateData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
