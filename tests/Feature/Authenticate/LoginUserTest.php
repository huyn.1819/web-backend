<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class LoginPageTest extends TestCase
{
    /** @test */
    public function user_login_successfully_if_validation_data_and_user_is_exist()
    {
        $user = $this->createUser();
        $loginData = ['email' => $user->email, 'password' => 'password'];
        $response = $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('status', Response::HTTP_OK)
                    ->where('message', 'User login successfully!!!')
                    ->where('data.user.id', $user->id)
                    ->where('data.user.name', $user->name)
                    ->where('data.user.phone', $user->phone)
                    ->whereType('data.access_token', 'string')
                    ->etc()
                );
        $this->assertAuthenticated();
    }

    /** @test */
    public function user_can_not_login_if_email_is_not_available()
    {
        $loginData = [
            'email' => "example@gmail.com",
            'password' => 'password'
        ];
        $response = $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('status', Response::HTTP_UNAUTHORIZED)
                    ->where('message', 'Unauthorized')
                );
    }

    /** @test */
    public function user_can_not_login_if_password_incorrect()
    {
        $user = $this->createUser();
        $loginData = ['email' => $user->email, 'password' => 'random'];
        $response = $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        $response
            ->assertJson(fn (AssertableJson $json) =>
            $json->where('status', Response::HTTP_UNAUTHORIZED)
                ->where('message', 'Unauthorized')
            );
    }

    /** @test */
    public function user_can_not_login_if_email_null()
    {
        $user = $this->createUser();
        $loginData = ['email' => "", 'password' => 'password'];
        $response = $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        $response
            ->assertJson(fn (AssertableJson $json) =>
            $json->where('status', Response::HTTP_UNAUTHORIZED)
                ->where('message', 'Unauthorized')
            );
    }

    /** @test */
    public function user_can_not_login_if_password_null()
    {
        $user = $this->createUser();
        $loginData = ['email' => $user->email, 'password' => ""];
        $response = $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        $response
            ->assertJson(fn (AssertableJson $json) =>
            $json->where('status', Response::HTTP_UNAUTHORIZED)
                ->where('message', 'Unauthorized')
            );
    }

}

