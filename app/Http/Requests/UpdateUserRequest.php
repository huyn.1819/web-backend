<?php

namespace App\Http\Requests;

class UpdateUserRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'password' => 'bail|string|min:8',
            'phone' => 'bail|digits:10'
        ];
    }
}
