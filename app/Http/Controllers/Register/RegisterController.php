<?php

namespace App\Http\Controllers\Register;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function register(Request $request)
    {
        $user = $this->userService->create($request);
        $user->assignRole($request->input('roles'));
        return $this->sendResponse($user, 'Sign up successfully!');
    }
}
