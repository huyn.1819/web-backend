<?php

use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Login\LoginController;
use App\Http\Controllers\Register\RegisterController;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return new UserResource($request->user());
});

//Route
Route::post('/signup', [RegisterController::class, 'register']);
Route::post('/login', [LoginController::class, 'login'])->name('login');

Route::middleware('auth:api')->post('/logout', [LoginController::class, 'logout']);

Route::group(['middleware' => 'auth:api'], function() {
    Route::name('users.')->prefix('users')->group(function () {
        Route::get('/', [UserController::class, 'index'])
            ->name('index')
            ->middleware('has_permission:user-list');
        Route::get('/show/{id}', [UserController::class, 'show'])
            ->name('show')
            ->middleware('has_permission:user-list');
        Route::post('/', [UserController::class, 'store'])
            ->name('store')
            ->middleware('has_permission:user-create');
        Route::put('/{id}', [UserController::class, 'update'])
            ->name('update')
            ->middleware('has_permission:user-edit');
        Route::delete('/{id}', [UserController::class, 'destroy'])
            ->name('destroy')
            ->middleware('has_permission:user-delete');

    });
    Route::name('roles.')->prefix('roles')->group(function () {
        Route::get('/', [RoleController::class, 'index'])
            ->name('index')
            ->middleware('has_permission:role-list');
        Route::get('/show/{id}', [RoleController::class, 'show'])
            ->name('show')
            ->middleware('has_permission:role-list');
        Route::post('/', [RoleController::class, 'store'])
            ->name('store')
            ->middleware('has_permission:role-create');
        Route::put('/{id}', [RoleController::class, 'update'])
            ->name('update')
            ->middleware('has_permission:role-edit');
        Route::delete('/{id}', [RoleController::class, 'destroy'])
            ->name('destroy')
            ->middleware('has_permission:role-delete');
    });
});
